<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="client/css/theme1.css" type="text/css">
</head>
<body>
	<form action="history" method="GET" class="form">
	<ul>
		<li>
		<input id="input" name="url" type="text" class="input_bar" autocomplete="off" spellcheck="false" autocorrect="off" autocapitalize="off" required="" placeholder="Введите url удалённого git-репозитория">
		</li>
		
		<li>
		От <input id="dt_start" name="dts" type="date" class="date_picker"> до <input id="dt_end" name="dte" type="date" class="date_picker">
		</li>
		
		<li>
		<input type="submit" value="Получить статистику">
		</li>
	</ul>
	</form >
</body>
</html>