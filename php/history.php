<?php
	const GIT_TOKEN			= 'a5ad0f33f5a7ae256969b6e57dd001ec3d8547f4';
	const PAGE_MAX_ROWS		= 20;
	
	set_time_limit(10000);
	
	$repo			= array_key_exists('url', $_GET) ? $_GET['url'] : null;
	$page			= array_key_exists('page', $_GET) ? intval($_GET['page']) : 0;
	$sort			= array_key_exists('sort', $_GET) ? $_GET['sort'] : null;
	$dateMin		= array_key_exists('dts', $_GET) ? strtotime($_GET['dts']) : 0;
	$dateMax		= array_key_exists('dte', $_GET) ? strtotime($_GET['dte']) : 0;
	
	$warn			= null;		// Сообщение об ошибке.
	$result			= array();	// Результат вывода скрипта. (HTML Таблица)
	$log			= array();  // Импровизированный журнал.
	$files			= array();	// Список файлов.
	$treesHash		= array();	// Отмечаем полученные хеши для предотвращения загрузки дубликатов.
	
	if ($repo === null)
		$warn = 'Удалённый репозиторий не был указан';
	else
		execute();
	
	// Получение данных:
	function execute(){
		global $warn, $repo;
		
		$param = parse_url($repo);
		
		if ($param === false || !array_key_exists('host', $param)) {
			$warn = 'Некорректный тип ссылки на репозиторий.';
			return;
		}
		
		switch ($param['host']) {
			case 'github.com':
				executeGitHub($param);
				return;
			default:
				$warn = 'Неподдерживаемый тип ссылки на репозиторий.';
				return;
		}
	}
	function executeGitHub(&$param) {
		global $files, $repo, $warn, $dateMin, $dateMax;
		
		// Формируем список файлов:
		$url		= 'https://api.github.com/repos'.$param['path'].'/contents';
		$resp		= gitHubLoadURL($url);
		
		if ($resp === null) {
			$warn	= 'Не удалось выполнить запрос.';
			return;
		}

		if (array_key_exists('message', $resp) && $resp['message'] == 'Not Found'){
			$warn	= 'Указанный репозиторий не найден на сервере.<br>'.$url;
			return;
		}
		
		// Рекурсивно получаем список всех файлов:
		gitHubGetFiles($url, $resp);
		
		// Получаем список всех коммитов:
		$url		= 'https://api.github.com/repos'.$param['path'].'/commits';
		$resp		= gitHubLoadURL($url);
		var_dump($resp);
		foreach($resp as $item) {
			$commit	= $item['commit'];
			$date	= strtotime($commit['committer']['date']);
			
			if ($dateMin > 0 && $dateMin > $date)
				continue;
			if ($dateMax > 0 && $dateMax < $date)
				continue;
			
			// Рекурсивно проходим все деревья коммита:
			gitHubGetTrees($commit['tree']['url'], $commit);
		}
	}
	function gitHubGetFiles($path, $resp){
		global $files;
		
		if ($resp === null)
			$resp	= gitHubLoadURL($path);
		if ($resp === null)
			return;
		
		foreach ($resp as $value) {
			if (gettype($value) === 'string') {
				$warn = "Получен некорректный ответ:<br>" . $value;
				return;
			}
			
			switch($value['type']) {
				case 'dir':
					gitHubGetFiles($value['url'], null);
					break;
				case 'file':
					$files[$value['path']] = new FileData($value['name'], $value['path']);
					break;
			}
		}
	}
	function gitHubGetTrees($path, $commit){
		global $files, $treesHash;
		
		$tree = gitHubLoadURL($path);
			
		foreach($tree['tree'] as $item){
			if($item['type'] === 'tree'){
				if (array_key_exists($item['sha'], $treesHash))
					continue; // Дубликаты не грузим.
				
				$treesHash[$item['sha']] = true;
				
				gitHubGetTrees($item['url'], $commit);
				continue;
			}
			
			$key = $item['path'];
			if(!array_key_exists($key, $files))
				continue; // Файла нет в последней версии репозитория.
			
			$file = $files[$item['path']];
			if ($file->sha === $item['sha'])
				continue; // Файл без изменений.
			
			$file->sha = $item['sha'];
			$file->commits ++;
			
			$author = $commit['author']['name'];
			if (array_key_exists($author, $file->changes))
				$file->changes[$author] ++;
			else
				$file->changes[$author] = 1;
		}
	}
	function gitHubLoadURL($url) {
		global $log;
		
		if (strpos($url, '?'))
			$url .= '&access_token=' . GIT_TOKEN;
		else
			$url .= '?access_token=' . GIT_TOKEN;
		
		array_push($log, "URL Request: $url");
		//try {
			$ch		= curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_TIMEOUT, '10'); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent:VolkovRA'));

			$resp 	= curl_exec($ch); 
			
			curl_close($ch);
			
			$resp	= json_decode($resp, true);
			/*
		}
		catch(Exception $err){
			$resp	= null;
		}
		*/
		return $resp;
	}
	
	// Сортировка:
	switch($sort){
		case 'pathU':
			uasort($files, 'sortPathUp');
			break;
		case 'pathD':
			uasort($files, 'sortPathDown');
			break;
			
		case 'commitsU':
			uasort($files, 'sortCommitsUp');
			break;
		case 'commitsD':
			uasort($files, 'sortCommitsDown');
			break;
		
		case 'authorsU':
			uasort($files, 'sortAuthorsUp');
			break;
		case 'authorsD':
			uasort($files, 'sortAuthorsDown');
			break;
		
		default:
			uasort($files, 'sortPathUp');
			break;
	}
	
	function sortPathUp($x, $y){
		if ($x->path > $y->path)
			return 1;
		if ($x->path < $y->path)
			return -1;
		
		return 0;
	}
	function sortPathDown($x, $y){
		if ($x->path > $y->path)
			return -1;
		if ($x->path < $y->path)
			return 1;
		
		return 0;
	}
	function sortCommitsUp($x, $y){
		if ($x->commits > $y->commits)
			return 1;
		if ($x->commits < $y->commits)
			return -1;
		
		if ($x->path > $y->path)
			return 1;
		if ($x->path < $y->path)
			return -1;
		
		return 0;
	}
	function sortCommitsDown($x, $y){
		if ($x->commits > $y->commits)
			return -1;
		if ($x->commits < $y->commits)
			return 1;
		
		if ($x->path > $y->path)
			return 1;
		if ($x->path < $y->path)
			return -1;
		
		return 0;
	}
	function sortAuthorsUp($x, $y){
		if (count($x->changes) > count($y->changes))
			return 1;
		if (count($x->changes) < count($y->changes))
			return -1;
		
		if ($x->path > $y->path)
			return 1;
		if ($x->path < $y->path)
			return -1;
		
		return 0;
	}
	function sortAuthorsDown($x, $y){
		if (count($x->changes) > count($y->changes))
			return -1;
		if (count($x->changes) < count($y->changes))
			return 1;
		
		if ($x->path > $y->path)
			return 1;
		if ($x->path < $y->path)
			return -1;
		
		return 0;
	}
	
	// Вывод таблицы:
	if ($warn === null)
		printTable();
	
	function printTable(){
		global $result, $page, $files;
		
		array_push($result, '<table class="result">');
		array_push($result, '<tr>');
		array_push($result, '<th class="num"></th>');
		array_push($result, '<th class="file">Файл'.getSortSpan('pathU', 'pathD').'</th>');
		array_push($result, '<th class="commits">Общее количество коммитов'.getSortSpan('commitsU', 'commitsD').'</th>');
		array_push($result, '<th class="authors">Список авторов и количество их коммитов'.getSortSpan('authorsU', 'authorsD').'</th>');
		array_push($result, '</tr>');
		
		$keys		= array_keys($files);
		$i			= $page * PAGE_MAX_ROWS;
		$limit		= PAGE_MAX_ROWS;
		$length		= count($keys);
		while($i < $length && $limit-- > 0){
			$file	= $files[$keys[$i++]];
			array_push($result, '<tr><td class="num">'.$i.'</td><td class="file">' . $file->path . '</td><td  class="commits">' . $file->commits . '</td><td class="authors">' . printChanges($file->changes) . '</td></tr>');
		}
		array_push($result, '</table>');
		
		// Вывод счётчика:
		if ($length > PAGE_MAX_ROWS){
			$pages	= floor($length / PAGE_MAX_ROWS + 1);
			
			array_push($result, '<ul class="stepper">');
			
			if ($page > 0)
				array_push($result, '<a href="'.getPageBack().'"><li class="prev">Назад</li></a>');
			
			array_push($result, '<li class="value">'.($page + 1).'/'.$pages.'</li>');
			
			if ($page + 1 < $pages)
				array_push($result, '<a href="'.getPageNext().'"><li class="prev">Вперёд</li></a>');
			
			array_push($result, '</ul>');
		}
	}
	function printChanges($changes){
		$arr	= array();
		$keys	= array_keys($changes);
		$length	= count($keys);
		$i		= 0;
		
		while ($i < $length){
			array_push($arr, '<p><span class="author">' . $keys[$i] . '</span><span class="count">' . $changes[$keys[$i]] . '</span></p>');
			$i++;
		}
		
		return join($arr);
	}
	function getPageBack(){
		global $page;
		
		$params		= array();
		
		if (array_key_exists('url', $_GET))		array_push($params, 'url='.$_GET['url']);
		array_push($params, 'page='.($page - 1));
		if (array_key_exists('sort', $_GET))	array_push($params, 'sort='.$_GET['sort']);
		if (array_key_exists('dts', $_GET))		array_push($params, 'dts='.$_GET['dts']);
		if (array_key_exists('dte', $_GET))		array_push($params, 'dte='.$_GET['dte']);
		
		$param		= parse_url($_SERVER['REQUEST_URI']);
		
		if (count($params) > 0)
			return $param['path'] . '?' . join('&', $params);
		else
			return $param['path'];
	}
	function getPageNext(){
		global $page;
		
		$params		= array();
		
		if (array_key_exists('url', $_GET))		array_push($params, 'url='.$_GET['url']);
		array_push($params, 'page='.($page + 1));
		if (array_key_exists('sort', $_GET))	array_push($params, 'sort='.$_GET['sort']);
		if (array_key_exists('dts', $_GET))		array_push($params, 'dts='.$_GET['dts']);
		if (array_key_exists('dte', $_GET))		array_push($params, 'dte='.$_GET['dte']);
		
		$param		= parse_url($_SERVER['REQUEST_URI']);
		
		if (count($params) > 0)
			return $param['path'] . '?' . join('&', $params);
		else
			return $param['path'];
	}
	function getSortSpan($type1, $type2){
		global $sort, $page;
		
		$params		= array();
		$param		= parse_url($_SERVER['REQUEST_URI']);
		
		if (array_key_exists('url', $_GET))		array_push($params, 'url='.$_GET['url']);
		if (array_key_exists('page', $_GET))	array_push($params, 'page='.$page);
		if (array_key_exists('dts', $_GET))		array_push($params, 'dts='.$_GET['dts']);
		if (array_key_exists('dte', $_GET))		array_push($params, 'dte='.$_GET['dte']);
		
		$href1		= $param['path'].'?'.join('&', $params)."&sort=$type1";
		$href2		= $param['path'].'?'.join('&', $params)."&sort=$type2";
		
		return '<span class="sorting"><span>'.($sort == $type1 ? 'Вверх' : '<a href="'.$href1.'">Вверх</a>').'</span><span>'.($sort == $type2 ? 'Вниз' : '<a href="'.$href2.'">Вниз</a>').'</span></span>';
	}
	
/// Данные файла.
class FileData
{
	public $name		= null;
	public $path		= null;
	public $commits		= 0;
	public $changes		= array();
	public $sha			= null;
	
	function __construct($name, $path) {
	   $this->name		= $name;
	   $this->path		= $path;
   }
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>History</title>
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="client/css/theme1.css" type="text/css">
</head>
<body>
	<?php
		if($warn !== null)		echo("<div class='warn'>$warn</div>");
		if(count($result) > 0)	echo("<div class='result'>" . join($result) . "</div>");
		if(count($log) > 0)		echo("<div class='log'>" . join('<br>', $log) . "</div>");
	?>
</body>
</html>