<?php
const INDEX_PHP			= 'index.php';
const PAGE_HOME			= '';
const PAGE_HISTORY		= 'history';

$map = array(
	PAGE_HOME			=> 'php' . DIRECTORY_SEPARATOR . 'home.php',
	PAGE_HISTORY		=> 'php' . DIRECTORY_SEPARATOR . 'history.php'
);

$root = getRoot();		// Корень сайта
$page = getPage();		// Запрошенный раздел

if (array_key_exists($page, $map))
	require(__DIR__ . DIRECTORY_SEPARATOR . $map[$page]);
else
	redirectToRoot();




function getRoot(){
	return substr($_SERVER['SCRIPT_NAME'], 0, -strlen(INDEX_PHP));
}
function getPage() {
	global $root;
	
	$str		= urldecode($_SERVER['REQUEST_URI']);
	$len		= strlen($root);
	$index		= strpos($str, '?', $len);
	
	if ($index === false)
		$str	= substr($str, $len);
	else
		$str	= substr($str, $len, $index - $len);
	
	if ($str === false)
		return '';
	else
		return $str;
}
function send404() {
	header("HTTP/1.0 404 Not Found");
	exit;
}
function send500() {
	header("HTTP/1.0 500 Internal Server Error");
	exit;
}
function redirectToRoot(){
	global $root;
	header("Location: $root");
	exit;
}
?>